import http from "../config/http"

export const dadosKid = (id) => http.get(`/users/kids/${id}`)

export const getAllKids = () => http.get("/users/kids")

export const createKid = (data) => http.post("/users/create" , data)

export const updateKid = (id, body) => http.put(`/users/kids/${id}`, body)

export const deleteKid = (id) => http.delete(`/users/kids/${id}`)
