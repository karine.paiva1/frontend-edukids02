import http from "../config/http"

export const plansGetAll = () => http.get("/plans")