import { MODAL } from "../types"

const INITIAL_STATE = {
  status: false,
  data: {}
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MODAL.CLOSE:
      return {...state, status: false, data:{}}
    case MODAL.TOGGLE:
      return {...state, status: !state.status, data: action.data}
    case MODAL.SET_DATA:
      return {...state, data: action.data}
    default :
      return state
  }
}

export default reducer