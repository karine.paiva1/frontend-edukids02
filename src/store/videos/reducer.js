import { VIDEOS } from "../types"

const INITIAL_STATE = {
  loading: false,
  videosUser: [],
  videosAll:[]
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case VIDEOS.VIDEOS_GET_ALL:
      return {
        ...state,
        videosUser: action.data
      }
    case VIDEOS.GET_VIDEOS:
      return {
        ...state,
        videosAll: action.data
      }
    default:
      return state
  }
}

export default reducer