import { getToken } from "../../config/storage";
import { LOGIN } from "../types";
import jwt from "jsonwebtoken";
// import { enumRoles } from "../../util/roles";

const INITIAL_STATE = {
  token: getToken() || "",
  user: jwt.decode(getToken()) || {},
  // type: enumRoles(jwt.decode(getToken())?.user_type) || {},
  loading: false,
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN.LOADING:
      return { ...state, loading: action.data };
    case LOGIN.LOGOUT:
      return { ...state, token: {}, user: {}, type: {} };
    case LOGIN.LOGIN:
      return {
        ...state,
        token: action.data.token,
        user: jwt.decode(action.data.token),
        // type: enumRoles(jwt.decode(action.data.token).user_type),
      };
    default:
      return state;
  }
};

export default reducer;