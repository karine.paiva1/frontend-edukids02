
export const getUrlYoutubeVideo = (url) => {

  const baseUrl = "https://www.youtube.com/embed/"
  let endUrl = ""

  if (url) {
    const video_id = url.split('v=')[1];
    const ampersandPosition = video_id.indexOf('&');
    if (ampersandPosition != -1) {
      video_id = video_id.substring(0, ampersandPosition);
    }
    endUrl = baseUrl + video_id
  }
    return endUrl;
}

