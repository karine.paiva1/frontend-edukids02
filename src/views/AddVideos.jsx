import { Input, Form, Button } from 'antd';
import { getAllVideos, dadosVideo, addVideos, updateVideo, deleteVideo } from '../services/service.videos';
import { useState } from 'react';
import '../components/portal/layout/style.css'


const AddVideos = () => {

  const [formVideosADM, setFormVideosADM] = useState({})

  const handleChange = (event) => {

    const { name, value } = event.target

    setFormVideosADM({
      ...formVideosADM,
      [name]: value

    })

  }

  const handleSubmit = async () => {

    await addVideos(formVideosADM)

  }

  return (
    <div className="formAddVideos">

      <Form.Item name="title" label="Title" rules={[
        {
          required: true,
          message: 'Digite o título.',
          whitespace: true,
        },
      ]}>
        <Input name="title" value={formVideosADM.title || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item name="video url" label="video url" rules={[
        {
          required: true,
          message: 'Digite a URL do vídeo!',
          whitespace: true,
        },
      ]}>
        <Input name="video_url" value={formVideosADM.video_url || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item name="thumb url" label="Thumb" rules={[
        {
          required: true,
          message: 'Digite o link da Thumb',
          whitespace: true,
        },
      ]}>
        <Input name="thumb_url" value={formVideosADM.thumb_url || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item name="description" label="Description" rules={[
        {
          required: true,
          message: 'Digite a descrição.',
          whitespace: true,
        },
      ]}>
        <Input name="description" value={formVideosADM.description || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item name="pg" label="pg" rules={[
        {
          type: 'number',
          required: true,
          message: 'Por favor, digite as idades.',
        },
      ]}>
        <Input name="pg" value={formVideosADM.pg || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item name="language" label="Language" rules={[
        {
          required: true,
          message: 'Digite a linguagem do vídeo.',
          whitespace: true,
        },
      ]}>
        <Input name="language" value={formVideosADM.language || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item name="category1" label="Category 1" rules={[
        {
          required: true,
          message: 'Digite a primeira categoria',
          whitespace: true,
        },
      ]}>
        <Input name="category1" value={formVideosADM.category1 || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item name="category2" label="Category 2" rules={[
        {
          required: true,
          message: 'Digite a segunda categoria',
          whitespace: true,
        },
      ]}>
        <Input name="category2" value={formVideosADM.category2 || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item name="category3" label="Category 3" rules={[
        {
          required: true,
          message: 'Digite a terceira categoria',
          whitespace: true,
        },
      ]}>
        <Input name="category3" value={formVideosADM.category3 || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" onClick={handleSubmit}>
          Adicionar
        </Button>
      </Form.Item>

    </div >
  )
}

export default AddVideos;