import { actionVideosGetAll } from "../store/videos/action";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react"
import { VideosAll } from "../components/videosAll"


const MyVideos = () => {
  const dispatch = useDispatch();
  const videosResult = useSelector((state) => state.video.videosUser)
  useEffect(() => {
    dispatch(actionVideosGetAll())
  }, [])


  return (
    <div className="containerVideos">
      
      <VideosAll videos={videosResult} />

    </div>


  )
}

export default MyVideos;