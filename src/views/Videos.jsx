import React, { useState, useEffect } from 'react'
import { Card, Divider } from 'antd';
import { useDispatch, useSelector } from "react-redux"
import { getVideos } from "../store/videos/action"
import { VideosAll } from "../components/videosAll"

function Videos(props) {
    const dispatch = useDispatch()
    const videosResult = useSelector((state) => state.video.videosAll)
    useEffect(()=> {
        dispatch(getVideos(true))
        
    },[])
  
  return (
    
      
      <VideosAll videos={videosResult} includeAddKid={true} />



  )
}

export { Videos }