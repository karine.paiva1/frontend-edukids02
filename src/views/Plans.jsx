import { useEffect, useState } from 'react'
import { plansGetAll } from "../services/service.plans"
import PlansList from "../components/plansAll"
import { subscriptionGetById } from "../services/service.subscription"

const Plans = () => {

  const [plans, setPlans] = useState([])
  const [subscriptionId, setSubscriptionId] = useState("")

  useEffect(() => {
    plansGetAll().then(result => {
      setPlans(result.data)
    }).catch(error => {
      alert(error.message)
    })

    subscriptionGetById().then(result => {
      setSubscriptionId(result.data?.plans_id || '')
    }).catch(error => {
      alert(error.message)
    })

  }, [])


  return (
    <div className="ContainerPlans">

      <PlansList plans={plans} subscriptionId={subscriptionId} />

    </div>
  )
}

export default Plans;