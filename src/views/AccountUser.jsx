import { Form, Input, Button } from 'antd';
import { dadosUser, updateUser, deleteUser } from '../services/service.accountuser';
import { useDispatch, useSelector } from 'react-redux'
import { useState, useEffect } from 'react';
import { navigate } from '@reach/router';
import Modal from "../components/modal"
import { logoutAuth } from "../store/login/action"


const AccountUser = () => {

  const dispatch = useDispatch()
  const [formUpdateUser, setUpdateFormUser] = useState({})
  const stateUser = useSelector(state => state.login.user)
  const [open, setOpen] = useState(false)

  const handleChange = (event) => {

    const { name, value } = event.target

    setUpdateFormUser({
      ...formUpdateUser,
      [name]: value,
    })

  }

  useEffect(() => {
    dadosUser(stateUser.id).then((result) => {
      setUpdateFormUser(result.data)
    }).catch((error) => {
      alert(error.message)
    })
  }, [])
  
  const logout = () => {
    dispatch(logoutAuth())
    setOpen(false)
  }

  const handleSubmit = async () => {

    try {

      await updateUser(stateUser.id, formUpdateUser)
      navigate('/myAccount', alert('Usuário atualizado com sucesso!'))

    } catch (error) {
      alert('Usuário não atualizado, tente novamente.')
    }

  }

  const handleDelete = async () => {

    try {
      await deleteUser(stateUser.id, deleteUser)
      navigate('/', alert ('Usuário excluído com sucesso.'))
    } catch (error) {
      alert('Não foi possível excluir sua conta, tente novamente.')
    }

  }

  const deletedUser = () => {
    logout();
    handleDelete();
  }

  return (

    <div className="form">

      <Input name="name" value={formUpdateUser.name || ""} onChange={handleChange} />

      <Input name="email" value={formUpdateUser.email || ""} onChange={handleChange} />

      <Input name="phone" value={formUpdateUser.phone || ""} onChange={handleChange} />

      <Input name="address" value={formUpdateUser.address || ""} onChange={handleChange} />

      <Form.Item>
        <Button type="primary" htmlType="submit" onClick={handleSubmit}>
          Atualizar
        </Button>
      </Form.Item>

      <Modal
        open={open}
        onCancel={() => setUpdateFormUser(false)}
      >
        <div>
          <p>
            Deseja realmente excluir sua conta?
          </p>
          <Button type="danger" onClick={deletedUser}>Sim</Button>
        </div>
      </Modal>
      <a key="delete" onClick={() => setOpen(true)}>Excluir conta</a>

    </div>
  )
}

export default AccountUser;