import { useSelector } from 'react-redux'
import { videosGetAll } from "../services/service.videos"
import { useEffect, useState } from 'react'
import Profile from '../components/kidsVideos/Profile'
import { Redirect } from '@reach/router'
import VideosList from '../components/kidsVideos/VideosList'
import Modal from '../components/modal/index'
import { getUrlYoutubeVideo } from '../utils/helpers'

export const KidsVideos = () => {

  const profile = useSelector((state) => state.kids.profile)
  const [profileVideo, setProfileVideo] = useState([])
  const [modalVideo, setModalVideo] = useState({ open: false, data: null })

  const openModal = (data) => {
    setModalVideo({ open: true, data })
  }

  useEffect(() => {

    if (profile.id) {
      videosGetAll(profile.id).then(result => {
        setProfileVideo(result.data)
      }).catch(error => {
        alert(error.message)
      })
    }
  }, [profile.id])

  if (!profile.id) {

    return <Redirect to="/" noThrow />

  }
  return (
    <div className="containerKidsVideos">

      <Modal
        title={modalVideo.data?.videos.title || ""}
        open={modalVideo.open}
        onCancel={() => {setModalVideo({open:false, data:null})}}>
          
                    <iframe id="ytplayer" type="text/html" width="480" height="260"
            src={getUrlYoutubeVideo(modalVideo.data?.videos.video_url)}
            frameborder="2" />
            
      </Modal>

      <Profile profile={profile} />
      <VideosList profileVideo={profileVideo} onClickCard={openModal}/>

    </div>
  )

}