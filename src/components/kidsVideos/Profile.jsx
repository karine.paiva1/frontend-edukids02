import { Row, Col } from 'antd';
import { Link } from '@reach/router';


const Profile = (props) => {


  return (


    <>

      <Row>
        <Col span={12}>

          <img src={props.profile.Avatar.avatar_url} alt="Avatar" style={{ width: '100px', height: '100px' }} />

        </Col>

        <Col span={12}>

          <p>{props.profile.nickname}</p>

        </Col>
       
      </Row>



    </>

  )
}






export default Profile;