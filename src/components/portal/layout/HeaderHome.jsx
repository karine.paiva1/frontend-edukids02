import { Button, Menu, Row, Col, Divider } from 'antd';
import { useDispatch, useSelector } from 'react-redux'
import { logoutAuth } from "../../../store/login/action"
import { useState } from "react"
import { isAuthenticated } from "../../../config/storage"
import { Link } from '@reach/router'
import Modal from "../../modal"
import Kids from "../../../components/videosAll/Kids"
import logoEDUKIDS from "../../../assets/images/logoEDUKIDS.png"

const { SubMenu } = Menu;

const HeaderHome = () => {
  const dispatch = useDispatch()
  const user = useSelector(state => state.login.user)
  const [open, setOpen] = useState(false)
  const logout = () => {
    dispatch(logoutAuth())
    setOpen(false)
  }

  const [selectedOption, setSelectedOption] = useState("")

  const handleClick = e => {
    console.log('click ', e);
    setSelectedOption(e.key);
  };

  return (
    <header>

      <Modal
        open={open}
        onCancel={() => setOpen(false)}
      >
        <div>
          <p>
            Deseja sair?
          </p>
          <Button type="danger" onClick={logout}>Sim</Button>
        </div>
      </Modal>

      <Row>
        <Col span={16}>
          <div className="logoHeader">
            <a href="/"><img src={logoEDUKIDS} width="100px" /></a>
          </div>
        </Col>
        <Col span={8} >
          <Menu onClick={handleClick} selectedKeys={[selectedOption]} mode="horizontal">
            {isAuthenticated() ?
              <SubMenu className="bgMenu" key="login" title={`Olá, ${user.name}`}>
                <Kids/>
                <Menu.Item key="videos"><Link to="/videos">Videos</Link></Menu.Item>
                <Menu.Item key="meusVideos"><Link to="/myVideos">Meus videos</Link></Menu.Item>
                <Menu.Item key="user"><Link to="/myAccount">Minha conta</Link></Menu.Item>
                <Menu.Item key="createkid"><Link to="/user/new">Cadastrar criança</Link></Menu.Item>
                <Menu.Item key="logout" onClick={() => setOpen(true)}>Logout</Menu.Item>
              </SubMenu>
              : <Menu.Item key="login"><Link to="/login">Entrar</Link></Menu.Item>}
          </Menu>
        </Col>
      </Row>


    </header >

  )
}

export default HeaderHome;