import { React, useEffect } from 'react'
import { useDispatch, useSelector } from "react-redux"
import { getKids, setKid } from "../../store/kids/action"
import { navigate } from "@reach/router";

function Kids() {
  const dispatch = useDispatch()
  const kids = useSelector((state) => state.kids.kids)

  useEffect(() => {
    dispatch(getKids())

  }, [])

const onClickKids = (kid) => {

  dispatch(setKid(kid))
  navigate('/user/profile')

}

  return (
    <ul>
      {kids.map(function (kid, i) {
        return (
          <li key={i} onClick={() => onClickKids(kid)}>
            <img width="80" height="80" src={kid.Avatar.avatar_url}></img>
            <p>{kid.nickname}</p>
          </li>
        )
      })}
    </ul>
  )
}

export default Kids
